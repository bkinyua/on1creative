# One Creative #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

One Creative WordPress Theme

Based off HTML5 Blank WordPress

### How do I get set up? ###

This uses Docker for development.

1. `git clone https://bkinyua@bitbucket.org/bkinyua/on1creative.git`
2. `docker-compose up -d`
3. `http://localhost:8000`

Alternatively, you can simply take the **on1creative** folder under **theme** and place it undr **wp-content/themes** it'll function normally

Points to note
____
1. It uses `SASS` for CSS therefore you'll need to compile. The Stylesheet is under **style.scss**
2. It uses `Bootstrap 4` stable

### Who do I talk to? ###

* Brian Kinyua <hello@briankinyua.com>