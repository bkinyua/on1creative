<?php get_header(); ?>
<div class="slideshow">
    <div class="slides">
	    <?php
	    $args = array(
		    'posts_per_page'   => -1,
		    'orderby'          => 'date',
		    'order'            => 'ASC',
		    'post_type'        => 'slide',
		    'post_status'      => 'publish'
	    );
	    $slides = get_posts( $args );
	    $count = 0;
	    foreach ( $slides as $post ) : setup_postdata( $post );
		    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
	    ?>
        <div class="slide <?php if($count < 1) echo 'slide--current'; ?>">
            <div class="slide__img" style="background-image: url('<?= esc_url($featured_img_url); ?>')"></div>
            <h2 class="slide__title">Conference Management</h2>
            <p class="slide__desc">A matter of delicate proportions and aesthetics.</p>
            <a class="slide__link" href="#">Explore our works</a>
        </div>
	    <?php $count++; endforeach;
	    wp_reset_postdata();?>
    </div>
    <nav class="slidenav">
        <button class="slidenav__item slidenav__item--prev">Previous</button>
        <span>/</span>
        <button class="slidenav__item slidenav__item--next">Next</button>
    </nav>
</div>
<article id="about">
    <header>
        <div class="container py-5 mt-4 text-center">
            <h1 class="font-weight-bold my-4 mx-auto position-relative d-inline-block">About Us</h1>
        </div>
    </header>
    <main class="py-4 text-center">
        <div class="container w-50">
	    <?php echo get_page_content(21); ?>
        </div>
    </main>
</article>
<article id="services" class="bg-light">
    <header class="pt-3">
        <div class="container py-5 mt-4 text-center">
            <h1 class="font-weight-bold my-4 mx-auto position-relative d-inline-block">Our Services</h1>
        </div>
    </header>
    <main class="py-4 text-center">
        <div class="container">
            <div class="row d-flex align-items-stretch">
			<?php
$args = array(
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'ASC',
	'post_type'        => 'service',
	'post_status'      => 'publish'
);
$services = get_posts( $args );
			foreach ( $services as $post ) : setup_postdata( $post ); ?>
                <div class="col-sm-12 col-md-3 d-flex mb-3" href="!<?php the_permalink(); ?>">
                    <div class="card w-100 rounded-0 service-item">
                        <img class="card-img-top" src="https://source.unsplash.com/600X500/?<?= str_replace("-",",",strtolower($post->post_name)); ?>" alt="<?php the_title(); ?>">
                        <div class="card-body">
                            <h3 class="card-title text-capitalize font-weight-bold"><?php the_title(); ?></h3>
                            <p class="card-text small d-none"><?php echo $post->post_content; ?></p>
                        </div>
                    </div>
                </div>
			<?php endforeach;
			wp_reset_postdata();?>
            </div>
        </div>
    </main>
</article>
<?php get_footer(); ?>
