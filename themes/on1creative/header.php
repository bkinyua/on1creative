<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if (wp_title('', false)) {
    echo ' :';
} ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Raleway:300,400,500" rel="stylesheet">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
		<div id="loading-screen" class="animated infinite pulse" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/on1-logo.png)"></div>

			<!-- header -->
			<header class="header clear position-absolute w-100" role="banner">

					<div id="brand">
                        <div class="container">
						<!-- logo -->
						<div class="logo py-3 pr-3 d-flex flex-row align-items-center text-white">
							<a href="<?php echo home_url(); ?>" class="d-inline-block p-2 mr-4 bg-white logo-link">
								<img src="<?php echo get_template_directory_uri(); ?>/img/on1-logo.png" alt="Logo" class="logo-img" height=65">
							</a>
							<div>
								<strong class="d-block h1 text-capitalize font-weight-bold pb-1 m-0"><?php echo bloginfo(); ?></strong>
								<p class="text-capitalize h4">Bespoke <span class="text-orange">concepts</span> and <span class="text-blue">events</span> management firm</p>
							</div>
                            <div class="ml-auto"><a href="" class="text-uppercase font-weight-bold d-inline-block btn btn-link btn-lg py-2 px-5 text-white">+254 720 123123</a><a href="" class="text-uppercase font-weight-bold d-inline-block btn btn-primary btn-lg rounded-0 text-white btn-cta py-2 px-4">Enquire</a></div>
						</div>
						<!-- /logo -->
                        </div>
					</div>

                <div class="bg-orange">
					<!-- nav -->
					<div class="container">
						<nav class="navbar navbar-expand-md py-0">
					   <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
					     <span class="navbar-toggler-icon"></span>
					   </button>
					   <?php html5blank_nav(); ?>
					 </nav>
					</div>
					<!-- /nav -->

                </div>
			</header>
			<!-- /header -->
